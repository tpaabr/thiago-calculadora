let $ = document;

function calcular(){
    let firstNum = parseInt($.getElementById("firstNum").value);
    let secondNum = parseInt($.getElementById("secondNum").value);

    let result;

    if(operator === "*") {
        result = firstNum * secondNum;
    }else if(operator === "/"){
        result = firstNum / secondNum;
    }else if(operator === "+"){
        result = firstNum + secondNum;
    }else {
        result = firstNum - secondNum;
    }

    guardarResultado(firstNum, secondNum, operator, result)
}

function guardarResultado(first, second, operation, res){
    console.log("chegou")
    let pai = $.getElementById("results")

    let resultSquare = $.createElement("div");
    resultSquare.classList.add("resultSquare")

    let newResult = $.createElement("p");
    newResult.innerText = `${first} ${operation} ${second} = ${res}`
    resultSquare.appendChild(newResult);
    

    let resultRemove = $.createElement("p");
    resultRemove.innerText = "Remove";
    resultRemove.classList.add("resultRemove");
    resultSquare.appendChild(resultRemove);

    pai.appendChild(resultSquare);
}

function reset(){
    let pai = $.getElementById("results");

    while (pai.firstChild) {
        pai.removeChild(pai.firstChild);
    }
}

function removeOne(e){
    resultList.removeChild(e.target);
}

function multiplyOp(){
    operator = "*"; 
}

function divideOp(){
    operator = "/";
}

function sumOp(){
    operator = "+";
}

function subOp(){
    operator = "-";
}

function reverse() {
    console.log("fo")
    let pai = $.getElementById("results");
    pai.style.flexDirection = "column-reverse";
}


let calcBut = $.getElementById("calcular");
let reverseBut = $.getElementById("reverse");
let resetBut = $.getElementById("reset");

let multiplyBut = $.getElementById("multiply");
let divideBut = $.getElementById("divide");
let sumBut = $.getElementById("sum");
let subBut = $.getElementById("subtraction");

let resultList = $.getElementById("results")
resultList.addEventListener("click", removeOne);

calcBut.addEventListener("click", calcular);
resetBut.addEventListener("click", reset);
reverseBut.addEventListener("click", reverse);

multiplyBut.addEventListener("click", multiplyOp);
divideBut.addEventListener("click", divideOp);
sumBut.addEventListener("click", sumOp);
subBut.addEventListener("click", subOp);